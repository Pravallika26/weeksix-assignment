package com.hcl.test;

import static org.junit.jupiter.api.Assertions.*;
import java.sql.Connection;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import com.hcl.database.DBConnection;

class JUnit5 {

	private Connection conn = null;

	// building connection
	@BeforeEach
	public void connectionFix() {
		this.conn = DBConnection.getInstance().getConnection();
	}

	// Testing connection
	@Test
	public void connectionTest() {
		assertNotNull(conn);
	}
}
