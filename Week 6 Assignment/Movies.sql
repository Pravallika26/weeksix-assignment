create database moviee;
use moviee;
create table moviedataset(id int(10)primary key not null,title varchar(45),year int(10),storyLine varchar(500),imdbRating int(10),Classification varchar(45));
desc moviedataset;

iNSERT INTO moviedataset VALUES (1,'Pushpa',2021,'Based on lives of red sandalwood smugglers',8,'Movies in Theatres');
iNSERT INTO moviedataset VALUES (2,'Avengers: Endgame',2019,'Avengers assemble once more to reverse Thanos actions',8,'top rated movies');
iNSERT INTO moviedataset VALUES (3,'Doctor Strange in the Multiverse of Madness',2022,'Dr.Stephen casts a forbidden spell that opens door to the multiverse',0,'coming soon');
iNSERT INTO moviedataset VALUES (4,'Shershaah',2021,'A biopic made on the heroic life of the 25-year-old brave Indian soldier Vikram Batra',9,'top rated indian');
iNSERT INTO moviedataset VALUES (5,'Thor: Love and Thunder',2022,'Odinson loses the ability to lift Mjolnir, and Jane Foster takes on the mantle of Thor, while simultaneously battling cancer in her human hours',0,'coming soon');
iNSERT INTO moviedataset VALUES (6,'Dil Bechara',2020,'Kizie and Manny are poles apart in personality and their battle against cancer is the only common thread binding them',8,'top rated indian');
iNSERT INTO moviedataset VALUES (7,'Jai Bhim',2021,'A pregnant woman from a primitive tribal community, searches desperately for her husband, who is missing from police custody. A High Court advocate rises in support to her',9,'top rated indian');
iNSERT INTO moviedataset VALUES (8,'Black Panther: Wakanda Forever',2022,'Wakanda Forever signified longevity and meaning well beyond the framework of the first film',0,'coming soon');
iNSERT INTO moviedataset VALUES (9,'Brahmastra',2022,'The film, based on Indian mythology, has been constructed as a trilogy.',0,'coming soon');
iNSERT INTO moviedataset VALUES (10,'Spider-Man:No Way home',2021,'When Peter asks for help from Doctor Strange, the stakes become even more dangerous, forcing him to discover what it truly means to be Spider-Man',9,'Movies in Theatres');
iNSERT INTO moviedataset VALUES (11,'Joker',2019,'Arthur Fleck, a party clown, leads an impoverished life with his ailing mother. However, when society shuns him and brands him as a freak,he decides to embrace the life of crime',8,'top rated movies');
iNSERT INTO moviedataset VALUES (12,'Jersey',2019,'Arjun, a talented but failed cricketer, decides to return to cricket in his late thirties, driven by the desire to represent the Indian cricket team and fulfil his sons wish',9,'top rated indian');
iNSERT INTO moviedataset VALUES (13,'Hero',2022,'Arjun an aspiring actor whose life takes a dramatic turn when he receives a gun in courier',8,'Movies in Theatres');


select * from moviedataset;